# Analizador PostgreSQL

Script python que permite analizar la base de datos de un esquema en específico del SGBD PostgreSQL, para extraer estadísticas de sus tablas y campos en una hoja de cálculo.
