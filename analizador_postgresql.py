#!/usr/bin/python3
# -*- coding: utf-8 -*-
# ========================================================================
#  ANALIZADOR DE ESQUEMAS Y ESTRUCTURAS DE TABLAS PARA EL SGBD POSTGRESQL
#
# Autores: - Joan Espinoza - Venezuela
#          - Franyer Hidalgo - Venezuela (fhidalgo.dev@gmail.com)
# Fecha: Julio 2020
#
# Este script permite conectarse a una base de datos PostgreSQL para
# extraer toda la estructura de las tablas de un esquema determinado,
# escribiendo todos los datos resultantes en una hoja de cálculo creada a
# partir del script.
# ========================================================================

# Se importa la Liberia de Gestión de BD PostgreSQL
import psycopg2
# Se importa la libreria para operar con hojas de calculo
import xlwt


class ConexionPgsql:
    """ Esta clase permite manejar la conexión de la base de datos postgresql """

    def __init__(self, dbname, user, host, password, port):
        self.dbname = dbname
        self.user = user
        self.host = host
        self.password = password
        self.port = port

    def ejecutarConsultaSQL(self, sql_query):
        """ Metodo para ejecutar consultas SQL sobre la base de datos especificada

            Parameters:
            :sql_query (str): String de la consulta que será ejecuta en BD

            Returns:
            :False (bool): En caso de que la conexión no sea exitosa se retorna False
            :registros (list): Lista de tuplas de los registros consultados
        """
        conexion = None
        try:
            conexion = psycopg2.connect(dbname=self.dbname,
                                        user=self.user,
                                        host=self.host,
                                        password=self.password,
                                        port=self.port)
            cursor = conexion.cursor()
            cursor.execute(sql_query)
            registros = cursor.fetchall()
            cursor.close()
            return registros
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            return False
        finally:
            if conexion is not None:
                conexion.close()


class ConsultaSQL(ConexionPgsql):
    """ Esta clase permite definir todas las consultas sql necesarias para
        obtener toda la estructura de la base de datos

        Parameters:
        :dbname (string): Nombre de la BD a conectar
        :user (string): Usuario con el que se conectará a la BD
        :host (string): Host con el que se conectará a la BD (Ejem: localhost)
        :password (string): Clave del usuario con el que se conectará a la BD
        :port (string): Puerto por el cual está corriendo Postgresql (Por defecto 5432)
    """

    def __init__(self, dbname, user, host, password, port):
        super().__init__(dbname, user, host, password, port)
        self.check = False  # Chequear la conexión a la BD
        # Lista de atributos con todos los esquemas de la conexión de la base de datos
        esquemas = self.ejecutarConsultaSQL(self.sql_consulta_esquemas())
        if esquemas:
            self.esquemas = [i[0] for i in esquemas]
            self.check = True

    def sql_consulta_esquemas(self):
        """ Retorna la consulta de los esquemas existentes en la BD """
        return """SELECT nspname FROM pg_catalog.pg_namespace
WHERE nspname NOT IN ('pg_toast', 'pg_temp_1',
                      'pg_toast_temp_1', 'pg_catalog',
                      'information_schema')"""

    def sql_consulta_1(self, esquema):
        """ Retorna la consulta SQL para listar las tablas de un esquema especifico

            Parameters:
            :esquema (str): String del esquema que será consultado
        """
        consulta = """-- Consulta SQL #1:
SELECT tablename FROM pg_catalog.pg_tables WHERE schemaname = '{0}' ORDER BY tablename;
""".format(esquema)
        return self.ejecutarConsultaSQL(consulta)

    def sql_consulta_2(self, esquema, tabla):
        """ Retorna la consulta SQL para devolver el conteo total de registros de una tabla

            Parameters:
            :esquema (str): String del esquema que será consultado
            :tabla (str): String de la tabla que será consultada
        """
        consulta = """-- Consulta SQL #2:
SELECT COUNT(*) FROM {0}.{1};
""".format(esquema, tabla)
        return self.ejecutarConsultaSQL(consulta)

    def sql_consulta_3(self, esquema, tabla):
        """ Retorna la consulta SQL para devolver el conteo total de campos de una tabla

            Parameters:
            :esquema (str): String del esquema que será consultado
            :tabla (str): String de la tabla que será consultada
        """
        consulta = """-- Consulta SQL #3:
SELECT COUNT(column_name)           --Seleccionamos el nombre de columna
FROM information_schema.columns     --Desde information_schema.columns
WHERE table_schema = '{0}'           --En el esquema que tenemos las tablas
AND table_name   = '{1}'             --El nombre de la tabla especifica de la que deseamos obtener información
""".format(esquema, tabla)
        return self.ejecutarConsultaSQL(consulta)

    def sql_consulta_4(self, esquema, tabla):
        """ Retorna la consulta SQL para devolver los atributos de los campos de una
            tabla para formar el diccionario de datos

            Parameters:
            :esquema (str): String del esquema que será consultado
            :tabla (str): String de la tabla que será consultada
        """
        consulta = """-- Consulta SQL #4:
SELECT column_name, data_type, character_maximum_length, is_nullable, is_identity
FROM information_schema.columns
WHERE table_schema = '{0}' AND table_name = '{1}'
""".format(esquema, tabla)
        return self.ejecutarConsultaSQL(consulta)

    def sql_consulta_5(self, esquema, tabla, criterio):
        """ Retorna la consulta SQL para devolver la cantidad de
            registros de una tabla que coinciden con un criterio

            Parameters:
            :esquema (str): String del esquema que será consultado
            :tabla (str): String de la tabla que será consultada
            :criterio (str): String del condicional WHERE de la consulta
        """
        consulta = """-- Consulta SQL #5:
SELECT COUNT(*) FROM {0}.{1} AS t WHERE {2};
""".format(esquema, tabla, criterio)
        return self.ejecutarConsultaSQL(consulta)

    def sql_consulta_6(self, esquema, tabla, campo, criterio):
        """ Retorna la consulta SQL para devolver la cantidad de registros segun
            la LONGITUD una Tabla

            Parameters:
            :esquema (str): String del esquema que será consultado
            :tabla (str): String de la tabla que será consultada
            :campo (str): String del campo a consultar en el condicional WHERE
            :criterio (str): String del condicional WHERE de la consulta
        """
        consulta = """-- Consulta SQL #6:
SELECT COUNT(*) FROM {0}.{1} AS t WHERE LENGTH(t.{2}) {3};
""".format(esquema, tabla, campo, criterio)
        return self.ejecutarConsultaSQL(consulta)


class HojaCalculo:
    """ Gestiona la hoja de calculo que se crea a partir de los datos consultados
        de la base de datos

        Parameters:
        :conexion (object): Instancia de la conexión con Postgresql
        :esquema (str): String del esquema que será operado en la consulta
        :formato (str): Formato de la hoja de calculo al crearse, por defecto ods
    """

    def __init__(self, conexion, esquema, formato='ods'):
        self.formato = formato
        self.tablas = conexion.sql_consulta_1(esquema)
        self.esquema = esquema
        self.documento = object
        self.conexion = conexion

    def estilos(self, documento):
        """ Gestiona los estilos de las celdas de la hoja de calculo

            Parameters:
            :documento (object): Instancia de la hoja de calculo creada

            Returns:
            :estilos (dict): Diccionario de los estilos que se aplicarán a la hoja de calculo
        """

        # bordes
        border = "borders: top_color black, bottom_color black, right_color black, left_color black, left hair, right hair, top hair, bottom hair;"
        # alineacion para titulos
        align = "align: wrap on, vert centre, horiz center;"

        # Estilo para titulo: negrita, centrado y con fondo color gris
        titulo = xlwt.easyxf("font: bold on; pattern: pattern solid, fore_colour gray25; {} {}".format(border, align))
        # Estilo numérico de miles 1.000 para los dígitos
        estilo_numerico = xlwt.easyxf(strg_to_parse=border, num_format_str='#,##0')

        estilos = {'estilo_numerico': estilo_numerico,
                   'titulo': titulo,
                   'bordes': xlwt.easyxf(border)
                   }
        return estilos

    def criterios_hoja3(self, tipo, tabla, criterio):
        # Criterio 1: el contenido del campo iterado es igual al literal 'null'
        if (tipo != 'integer') and (tipo != 'bigint') and (tipo != 'numeric') and (tipo != 'bytea') and (tipo != 'double precision') and (tipo != 'boolean') and (tipo != 'date') and (tipo != 'timestamp without time zone'):
            coincidentes = self.conexion.sql_consulta_5(self.esquema, tabla, criterio)
            resultado = str(coincidentes[0][0])
        else:
            resultado = "No Aplica"
        return resultado

    def crear_documento(self):
        """ Crea el documento de la hoja de calculo """
        nombre_documento = self.conexion.dbname
        print('Creando documento: {}_{}.{}\n'.format(nombre_documento, self.esquema, self.formato))
        self.documento = xlwt.Workbook(encoding="utf8")
        print("\n\tCreando Hoja 1: TABLAS DEL ESQUEMA")
        self.hoja_1(self.documento)
        print("\n\tCreando Hoja 2: DICCIONARIO DE DATOS")
        self.hoja_2(self.documento)
        print("\n\tCreando Hoja 3: DATOS NULOS")
        self.hoja_3(self.documento)
        print("\n\tCreando Hoja 4: LONGITUDES DE CAMPOS")
        self.hoja_4(self.documento)
        self.documento.save("{0}_{1}.{2}".format(nombre_documento, self.esquema, self.formato))

    def hoja_1(self, documento):
        """ Se ensambla en la primera hoja la cantidad de registros y
            de campos de cada tabla del esquema seleccionado

            Parameters:
            :documento (object): Instancia de la hoja de calculo creada

            Returns:
            :hoja1 (object): Instancia de la primera hoja del documento
        """
        estilos = self.estilos(documento)
        hoja1 = documento.add_sheet("TABLAS DEL ESQUEMA")
        hoja1.write_merge(0, 0, 0, 2, "TABLAS DEL ESQUEMA: {}".format(str(self.esquema).upper()), style=estilos.get('titulo'))
        hoja1.col(0).width = len("NOMBRE DE LA TABLA") * 367
        hoja1.write(1, 0, "NOMBRE DE LA TABLA", style=estilos.get('titulo'))
        hoja1.write(1, 1, "REGISTROS", style=estilos.get('titulo'))
        hoja1.write(1, 2, "CAMPOS", style=estilos.get('titulo'))
        fila = 2
        for tabla in self.tablas:
            registros = self.conexion.sql_consulta_2(self.esquema, tabla[0])
            campos = self.conexion.sql_consulta_3(self.esquema, str(tabla[0]))
            # Escribiendo los datos en la hoja de calculo
            hoja1.write(fila, 0, tabla[0], style=estilos.get('bordes'))
            hoja1.write(fila, 1, int(registros[0][0]), estilos.get('estilo_numerico'))
            hoja1.write(fila, 2, int(campos[0][0]), estilos.get('estilo_numerico'))
            fila += 1
        return hoja1

    def hoja_2(self, documento):
        """ Se ensambla en la segunda hoja el diccionario de datos de cada tabla
            del esquema seleccionado

            Parameters:
            :documento (object): Instancia de la hoja de calculo creada

            Returns:
            :hoja2 (object): Instancia de la segunda hoja del documento
        """
        estilos = self.estilos(documento)
        hoja2 = documento.add_sheet("DICCIONARIO DE DATOS")
        columna_inicio = 0
        columna_fin = 5
        fila_mayor = 2
        fila_encabezado = 0
        for tabla in self.tablas:
            fila = 2
            if columna_fin >= 250 and columna_fin <= 255:
                fila_encabezado = fila_mayor + 2
                columna_inicio = 0
                columna_fin = 5
            if fila_encabezado > 0:
                fila = fila_encabezado + 2

            hoja2.write_merge(fila_encabezado, fila_encabezado, columna_inicio, columna_fin - 1, "DICCIONARIO DE LA TABLA: {}".format(str(tabla[0]).upper()), style=estilos.get('titulo'))
            hoja2.col(columna_inicio).width = len("NOMBRE DEL CAMPO") * 367
            hoja2.write(fila_encabezado + 1, columna_inicio, "NOMBRE DEL CAMPO", style=estilos.get('titulo'))
            hoja2.col(columna_inicio + 1).width = len("TIPO DE DATO") * 387
            hoja2.write(fila_encabezado + 1, columna_inicio + 1, "TIPO DE DATO", style=estilos.get('titulo'))
            hoja2.write(fila_encabezado + 1, columna_inicio + 2, "LONGITUD", style=estilos.get('titulo'))
            hoja2.write(fila_encabezado + 1, columna_inicio + 3, u"¿NULL?", style=estilos.get('titulo'))
            hoja2.write(fila_encabezado + 1, columna_inicio + 4, u"¿ID?", style=estilos.get('titulo'))

            nombres_campos = self.conexion.sql_consulta_4(self.esquema, str(tabla[0]))

            for campo in nombres_campos:
                if campo[2] is None:
                    longitud = str("-")
                else:
                    longitud = str(campo[2])
                puede_nulo = str(campo[3])
                es_identidad = str(campo[4])

                hoja2.write(fila, columna_inicio, campo[0], style=estilos.get('bordes'))
                hoja2.write(fila, columna_inicio + 1, campo[1], style=estilos.get('bordes'))
                hoja2.write(fila, columna_inicio + 2, longitud, style=estilos.get('bordes'))
                hoja2.write(fila, columna_inicio + 3, puede_nulo, style=estilos.get('bordes'))
                hoja2.write(fila, columna_inicio + 4, es_identidad, style=estilos.get('bordes'))

                fila += 1
                if fila_mayor < fila:
                    fila_mayor = fila
            columna_inicio += 6
            columna_fin += 6
        return hoja2

    def hoja_3(self, documento):
        """ Se ensambla en la tercera hoja los indicadores derivados del
            analisis del contenido de los campos de cada Tabla

            Parameters:
            :documento (object): Instancia de la hoja de calculo creada

            Returns:
            :hoja3 (object): Instancia de la tercera hoja del documento
        """
        estilos = self.estilos(documento)
        hoja3 = documento.add_sheet("DATOS NULOS")
        columna_inicio = 0
        columna_fin = 4
        fila_mayor = 2
        fila_encabezado = 0
        for tabla in self.tablas:
            fila = 2

            if columna_fin >= 250 and columna_fin <= 255:
                fila_encabezado = fila_mayor + 2
                columna_inicio = 0
                columna_fin = 5

            if fila_encabezado > 0:
                fila = fila_encabezado + 2

            titulo = u"ANÁLISIS DE CAMPOS DE LA TABLA: {}".format(str(tabla[0]).upper())
            hoja3.write_merge(fila_encabezado, fila_encabezado, columna_inicio, columna_fin - 1, titulo, style=estilos.get('titulo'))
            hoja3.col(columna_inicio).width = len("NOMBRE DEL CAMPO") * 397
            hoja3.write(fila_encabezado + 1, columna_inicio, "NOMBRE DEL CAMPO", style=estilos.get('titulo'))
            hoja3.write(fila_encabezado + 1, columna_inicio + 1, "'null'", style=estilos.get('titulo'))
            hoja3.write(fila_encabezado + 1, columna_inicio + 2, "NULL", style=estilos.get('titulo'))
            hoja3.write(fila_encabezado + 1, columna_inicio + 3, "VACIOS", style=estilos.get('titulo'))

            nombres_campos = self.conexion.sql_consulta_4(self.esquema, str(tabla[0]))
            for campo in nombres_campos:
                nombre = 't.' + str(campo[0])
                tipo = str(campo[1])

                # Criterio 1: el contenido del campo iterado es igual al literal 'null'
                criterio = nombre + " = 'null'"
                c1 = self.criterios_hoja3(tipo, str(tabla[0]), criterio)
                # Criterio 2: el contenido del campo iterado es NULL
                criterio = nombre + " IS NULL"
                c2 = self.criterios_hoja3("False", str(tabla[0]), criterio)
                # Criterio 3: el contenido del campo iterado es igual a VACIO
                criterio = "TRIM(" + nombre + ") = ''"
                c3 = self.criterios_hoja3(tipo, str(tabla[0]), criterio)

                hoja3.write(fila, columna_inicio, nombre.replace('t.', ''), style=estilos.get('bordes'))
                if c1.isdigit():
                    hoja3.write(fila, columna_inicio + 1, int(c1), estilos.get('estilo_numerico'))
                else:
                    hoja3.write(fila, columna_inicio + 1, c1, style=estilos.get('bordes'))
                if c2.isdigit():
                    hoja3.write(fila, columna_inicio + 2, int(c2), estilos.get('estilo_numerico'))
                else:
                    hoja3.write(fila, columna_inicio + 2, c2, style=estilos.get('bordes'))
                if c3.isdigit():
                    hoja3.write(fila, columna_inicio + 3, int(c3), estilos.get('estilo_numerico'))
                else:
                    hoja3.write(fila, columna_inicio + 3, c3, style=estilos.get('bordes'))

                fila += 1
                if fila_mayor < fila:
                    fila_mayor = fila

            columna_inicio += 5
            columna_fin += 5
        return hoja3

    def hoja_4(self, documento):
        """ Se ensambla en la tercera hoja los indicadores derivados del analisis
            de la longitud de campos en cada Tabla

            Parameters:
            :documento (object): Instancia de la hoja de calculo creada

            Returns:
            :hoja4 (object): Instancia de la cuarta hoja del documento
        """
        estilos = self.estilos(documento)
        hoja4 = documento.add_sheet("LONGITUDES DE LOS CAMPOS")
        columna_inicio = 0
        columna_fin = 8
        fila_mayor = 2
        fila_encabezado = 0
        for tabla in self.tablas:
            fila = 2
            if columna_fin >= 250 and columna_fin <= 255:
                fila_encabezado = fila_mayor + 2
                columna_inicio = 0
                columna_fin = 9
            if fila_encabezado > 0:
                fila = fila_encabezado + 2
            titulo = u"ANÁLISIS LA CANTIDAD DE REGISTROS SEGÚN LA LONGITUD DE LOS CAMPOS ( Tabla: {})".format(str(tabla[0]).upper())

            # Calcular los anchos de las columnas
            hoja4.col(columna_inicio).width = len("NOMBRE DEL CAMPO") * 367
            hoja4.col(columna_inicio + 1).width = (len(titulo) * 367) // 7
            hoja4.col(columna_inicio + 2).width = (len(titulo) * 367) // 7
            hoja4.col(columna_inicio + 3).width = (len(titulo) * 367) // 7
            hoja4.col(columna_inicio + 4).width = (len(titulo) * 367) // 7
            hoja4.col(columna_inicio + 5).width = (len(titulo) * 367) // 7
            hoja4.col(columna_inicio + 6).width = (len(titulo) * 367) // 7
            hoja4.col(columna_inicio + 7).width = (len(titulo) * 367) // 7
            hoja4.col(columna_inicio + 8).width = (len(titulo) * 367) // 7
            # Escribir en la hoja de calculo
            hoja4.write_merge(fila_encabezado, fila_encabezado, columna_inicio, columna_fin - 1, titulo, style=estilos.get('titulo'))
            hoja4.write(fila_encabezado + 1, columna_inicio, "NOMBRE DEL CAMPO", style=estilos.get('titulo'))
            hoja4.write(fila_encabezado + 1, columna_inicio + 1, "0", style=estilos.get('titulo'))
            hoja4.write(fila_encabezado + 1, columna_inicio + 2, "1", style=estilos.get('titulo'))
            hoja4.write(fila_encabezado + 1, columna_inicio + 3, "2", style=estilos.get('titulo'))
            hoja4.write(fila_encabezado + 1, columna_inicio + 4, "3", style=estilos.get('titulo'))
            hoja4.write(fila_encabezado + 1, columna_inicio + 5, "4", style=estilos.get('titulo'))
            hoja4.write(fila_encabezado + 1, columna_inicio + 6, "5", style=estilos.get('titulo'))
            hoja4.write(fila_encabezado + 1, columna_inicio + 7, ">=6", style=estilos.get('titulo'))

            nombres_campos = self.conexion.sql_consulta_4(self.esquema, str(tabla[0]))
            for campo in nombres_campos:
                nombre = str(campo[0])
                tipo = str(campo[1])

                criterios = {'c0': '',
                             'c1': '',
                             'c2': '',
                             'c3': '',
                             'c4': '',
                             'c5': '',
                             'c6': '', }
                for i in range(0, 7):
                    c = 'c{}'.format(i)
                    if (tipo != 'integer') and (tipo != 'bigint') and (tipo != 'numeric') and (tipo != 'bytea') and (tipo != 'double precision') and (tipo != 'boolean') and (tipo != 'date') and (tipo != 'timestamp without time zone'):
                        criterio = '= {}'.format(i)
                        if i == 6:
                            criterio = '>= {}'.format(i)
                        coincidentes = self.conexion.sql_consulta_6(self.esquema, str(tabla[0]), nombre, criterio)
                        if coincidentes:
                            criterios[c] = str(coincidentes[0][0])
                        else:
                            criterios[c] = '-'
                    else:
                        criterios[c] = 'No Aplica'

                hoja4.write(fila, columna_inicio, nombre, style=estilos.get('bordes'))
                if criterios.get('c0').isdigit():
                    hoja4.write(fila, columna_inicio + 1, int(criterios.get('c0')), estilos.get('estilo_numerico'))
                else:
                    hoja4.write(fila, columna_inicio + 1, criterios.get('c0'), style=estilos.get('bordes'))
                if criterios.get('c1').isdigit():
                    hoja4.write(fila, columna_inicio + 2, int(criterios.get('c1')), estilos.get('estilo_numerico'))
                else:
                    hoja4.write(fila, columna_inicio + 2, criterios.get('c1'), style=estilos.get('bordes'))
                if criterios.get('c2').isdigit():
                    hoja4.write(fila, columna_inicio + 3, int(criterios.get('c2')), estilos.get('estilo_numerico'))
                else:
                    hoja4.write(fila, columna_inicio + 3, criterios.get('c2'), style=estilos.get('bordes'))
                if criterios.get('c3').isdigit():
                    hoja4.write(fila, columna_inicio + 4, int(criterios.get('c3')), estilos.get('estilo_numerico'))
                else:
                    hoja4.write(fila, columna_inicio + 4, criterios.get('c3'), style=estilos.get('bordes'))
                if criterios.get('c4').isdigit():
                    hoja4.write(fila, columna_inicio + 5, int(criterios.get('c4')), estilos.get('estilo_numerico'))
                else:
                    hoja4.write(fila, columna_inicio + 5, criterios.get('c4'), style=estilos.get('bordes'))
                if criterios.get('c5').isdigit():
                    hoja4.write(fila, columna_inicio + 6, int(criterios.get('c5')), estilos.get('estilo_numerico'))
                else:
                    hoja4.write(fila, columna_inicio + 6, criterios.get('c5'), style=estilos.get('bordes'))
                if criterios.get('c6').isdigit():
                    hoja4.write(fila, columna_inicio + 7, int(criterios.get('c6')), estilos.get('estilo_numerico'))
                else:
                    hoja4.write(fila, columna_inicio + 7, criterios.get('c6'), style=estilos.get('bordes'))

                fila += 1
                if fila_mayor < fila:
                    fila_mayor = fila

            columna_inicio += 9
            columna_fin += 9
        return hoja4


class Launcher:
    """ Ejecuta la aplicación gestionando toda la salida de opciones amigables
        por la consola
    """

    def __init__(self):
        self.bienvenida()
        self.datos_conexion()

    def formatear_mensaje(self, decoracion, msg):
        """ Formatear un mensaje colocandole un header y un footer según su decoración

            Parameters:
            :decoracion (str): Caracter de decoración del mensaje
            :msg (str): Cadena de caracteres que se formateará

            Returns:
            :mensaje (str): Mensaje formateado
        """
        decoracion = decoracion * 80
        mensaje = """"""

        mensaje += """{0}\n\n{1}\n\n{0}""".format(decoracion, msg)
        return mensaje

    def bienvenida(self):
        """ Mostrar mensaje de bienvenida """''
        msg = "\t\tBIENVENIDOS AL ANALIZADOR DE POSTGRESQL\n"
        msg += "\nEl script analizará de todas las tablas de su esquema de base de datos, lo siguiente:"
        msg += "\n\t1-TABLAS DEL ESQUEMA"
        msg += "\n\t2-DICCIONARIO DE DATOS"
        msg += "\n\t3-CANTIDAD DE DATOS NULOS SEGÚN LOS REGISTROS DE CADA CAMPO EN SUS TABLAS"
        msg += "\n\t4-LONGITUD DE LOS REGISTROS DE CADA CAMPO EN SUS TABLAS"
        msg += "\nToda la información será generada en un archivo binario de hoja de calculo con el nombre de su base de datos y esquema analizado\n"
        print(self.formatear_mensaje('=', msg))

    def datos_conexion(self):
        """ Pide al usuario los datos necesarios para la conexión """
        print("Ingresa la siguiente información\n")
        nombre_bd = input("Base de Datos: ".strip())
        usuario = input("Usuario: ".strip())
        password = input("Contraseña: ".strip())
        host = input("Host: ".strip())
        puerto = input("Puerto: ".strip())
        conexion = ConsultaSQL(nombre_bd, usuario, host, password, puerto)
        if conexion.check:
            msg = "¡Conexión Exitosa!"
            print(self.formatear_mensaje('=', msg))
            self.seleccion_esquemas(conexion)
        else:
            msg = "Ha ocurrido un error al conectar. ¡Vuelve a intentarlo!"
            print(self.formatear_mensaje('=', msg))
            self.datos_conexion()

    def seleccion_esquemas(self, conexion):
        """ Gestiona operaciones con base de datos multiesquemas

            Parameters:
            :conexion (object): Instancia de la conexión con Postgresql
        """
        if len(conexion.esquemas) > 1:
            print("Selecciona el esquema que deseas analizar: \n")
            contador = 0
            for i in conexion.esquemas:
                print('{1}-{0}\n'.format(i, contador))
                contador += 1
            opcion_esquema = int(input("Las opciones validas van desde el rango número 0 hasta el {}: ".format(contador - 1)))
            if opcion_esquema not in range(0, contador):
                msg = "¡Opción no valida!"
                print(self.formatear_mensaje('=', msg))
                self.seleccion_esquemas(conexion)
            else:
                self.analizar_esquema(conexion, conexion.esquemas[opcion_esquema])
        else:
            print("El único esquema existente es el {}, por lo tanto se analizará solo ese".format(conexion.esquemas[0]))
            self.analizar_esquema(conexion, conexion.esquemas[0])

    def analizar_esquema(self, conexion, esquema):
        """ Permite analizar el esquema elegido y escribirlo en la hoja de
            calculo

            Parameters:
            :conexion (object): Instancia de la conexión con Postgresql
            :esquema (str): String del esquema que será analizado
        """
        msg = "Analizando esquema {}".format(esquema)
        print(self.formatear_mensaje('=', msg))
        HojaCalculo(conexion, esquema).crear_documento()


if __name__ == "__main__":
    Launcher()
